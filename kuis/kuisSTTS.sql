-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 03 Des 2016 pada 14.42
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `savsoftquiz`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `class_coment`
--

CREATE TABLE `class_coment` (
  `content_id` int(11) NOT NULL,
  `generated_time` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `content_by` int(11) NOT NULL,
  `published` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `class_coment`
--

INSERT INTO `class_coment` (`content_id`, `generated_time`, `content`, `content_by`, `published`, `class_id`) VALUES
(1, 1480747448, 'asd', 1, 1, 1),
(2, 1480747509, 'Saya mau tanya pak', 14, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `class_gid`
--

CREATE TABLE `class_gid` (
  `clgid` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `difficult_level`
--

CREATE TABLE `difficult_level` (
  `did` int(11) NOT NULL,
  `level_name` varchar(100) NOT NULL,
  `institute_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `difficult_level`
--

INSERT INTO `difficult_level` (`did`, `level_name`, `institute_id`) VALUES
(1, 'Easy', 1),
(2, 'Medium', 1),
(3, 'Difficult', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `essay_qsn`
--

CREATE TABLE `essay_qsn` (
  `essay_id` int(11) NOT NULL,
  `q_id` int(11) NOT NULL,
  `r_id` int(11) NOT NULL,
  `essay_cont` longtext NOT NULL,
  `essay_score` int(11) NOT NULL,
  `essay_status` int(11) NOT NULL DEFAULT '0',
  `q_type` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `essay_qsn`
--

INSERT INTO `essay_qsn` (`essay_id`, `q_id`, `r_id`, `essay_cont`, `essay_score`, `essay_status`, `q_type`) VALUES
(1, 6, 1, 'Red=Green,BMW=Honda,Keyboard=Mouse,Eye=Nose', 0, 0, 5),
(2, 4, 1, 'Red', 0, 0, 3),
(3, 6, 1, 'Red=Green,BMW=Honda,Keyboard=Mouse,Eye=Nose', 0, 0, 5),
(4, 3, 1, 'Transparent', 0, 0, 2),
(5, 6, 2, 'Red=Mouse,BMW=Nose,Keyboard=Green,Eye=Honda', 0, 0, 5),
(6, 4, 2, 'red', 0, 0, 3),
(7, 3, 2, 'yellow', 0, 0, 2),
(8, 6, 2, 'Red=Mouse,BMW=Nose,Keyboard=Green,Eye=Honda', 0, 0, 5),
(9, 5, 2, 'India is a country in Asia', 1, 1, 0),
(10, 6, 3, 'Red=Nose,BMW=Green,Keyboard=Green,Eye=Mouse', 0, 0, 5),
(11, 4, 3, 'blue', 0, 0, 3),
(12, 3, 3, 'transparent', 0, 0, 2),
(13, 6, 3, 'Red=Nose,BMW=Green,Keyboard=Green,Eye=Mouse', 0, 0, 5),
(14, 5, 3, 'india india', 1, 1, 0),
(15, 6, 4, 'Red=Mouse,BMW=Nose,Keyboard=Honda,Eye=Mouse', 0, 0, 5),
(16, 4, 4, 'blue', 0, 0, 3),
(17, 3, 4, 'transparent', 0, 0, 2),
(18, 6, 4, 'Red=Mouse,BMW=Nose,Keyboard=Honda,Eye=Mouse', 0, 0, 5),
(19, 5, 4, 'india', 1, 1, 0),
(20, 6, 5, 'Red=Nose,BMW=Green,Keyboard=Honda,Eye=Mouse', 0, 0, 5),
(21, 4, 5, 'blue', 0, 0, 3),
(22, 3, 5, 'transparent', 0, 0, 2),
(23, 6, 5, 'Red=Nose,BMW=Green,Keyboard=Honda,Eye=Mouse', 0, 0, 5),
(24, 5, 5, 'india', 1, 1, 0),
(25, 6, 6, 'Red=,BMW=,Keyboard=,Eye=', 0, 0, 5),
(26, 4, 6, 'blue', 0, 0, 3),
(27, 3, 6, 'transparent', 0, 0, 2),
(28, 5, 6, 'india', 1, 1, 0),
(29, 6, 7, 'Red=Nose,BMW=Green,Keyboard=Nose,Eye=Green', 0, 0, 5),
(30, 4, 7, 'blue', 0, 0, 3),
(31, 3, 7, 'transparent', 0, 0, 2),
(32, 6, 7, 'Red=Nose,BMW=Green,Keyboard=Nose,Eye=Green', 0, 0, 5),
(33, 5, 7, 'india is a country', 1, 1, 0),
(34, 6, 8, 'Red=Honda,BMW=Green,Keyboard=Green,Eye=Green', 0, 0, 5),
(35, 4, 8, 'blue', 0, 0, 3),
(36, 3, 8, 'transparent', 0, 0, 2),
(37, 6, 8, 'Red=Honda,BMW=Green,Keyboard=Green,Eye=Green', 0, 0, 5),
(38, 5, 8, 'india', 1, 1, 0),
(39, 4, 9, '', 0, 0, 3),
(40, 3, 9, '', 0, 0, 2),
(41, 6, 10, 'Red=Mouse,Keyboard=,Eye=,BMW=', 0, 0, 5),
(42, 4, 10, 'hhhh', 0, 0, 3),
(43, 6, 10, 'Red=Mouse,Keyboard=,Eye=,BMW=', 0, 0, 5),
(44, 3, 10, 'bbb', 0, 0, 2),
(45, 4, 11, '', 0, 0, 3),
(46, 3, 11, '', 0, 0, 2),
(47, 6, 12, 'Keyboard=Mouse,BMW=Honda,Red=Green,Eye=Nose', 0, 0, 5),
(48, 4, 12, 'Blue', 0, 0, 3),
(49, 3, 12, 'blue', 0, 0, 2),
(50, 6, 12, 'Keyboard=Mouse,BMW=Honda,Red=Green,Eye=Nose', 0, 0, 5),
(51, 8, 12, 'Halooooooooooooo', 0, 0, 0),
(52, 4, 13, '', 0, 0, 3),
(53, 3, 13, '', 0, 0, 2),
(54, 6, 14, 'Red=Green,BMW=Honda,Eye=Nose,Keyboard=Mouse', 0, 0, 5),
(55, 4, 14, 'Blue', 0, 0, 3),
(56, 3, 14, 'Blue', 0, 0, 2),
(57, 6, 14, 'Red=Green,BMW=Honda,Eye=Nose,Keyboard=Mouse', 0, 0, 5),
(58, 5, 14, 'Hello, this is an essay', 100, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gcm_ids`
--

CREATE TABLE `gcm_ids` (
  `gcm_id` int(11) NOT NULL,
  `gcm_regid` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `institute_data`
--

CREATE TABLE `institute_data` (
  `su_institute_id` int(11) NOT NULL,
  `organization_name` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL DEFAULT 'logo.png',
  `contact_info` text NOT NULL,
  `active_till` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `description` text NOT NULL,
  `custom_domain` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `institute_data`
--

INSERT INTO `institute_data` (`su_institute_id`, `organization_name`, `logo`, `contact_info`, `active_till`, `status`, `description`, `custom_domain`) VALUES
(1, '', 'logo.png', '', 2147483647, 1, '', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `live_class`
--

CREATE TABLE `live_class` (
  `class_id` int(11) NOT NULL,
  `class_name` varchar(1000) NOT NULL,
  `initiated_by` int(11) NOT NULL,
  `initiated_time` int(11) NOT NULL,
  `closed_time` int(11) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `notifications`
--

CREATE TABLE `notifications` (
  `nid` int(11) NOT NULL,
  `gid` int(11) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `noti_date` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `paypal_ipn`
--

CREATE TABLE `paypal_ipn` (
  `id` int(11) NOT NULL,
  `itransaction_id` varchar(60) NOT NULL,
  `ipayerid` varchar(60) NOT NULL,
  `iname` varchar(60) NOT NULL,
  `iemail` varchar(60) NOT NULL,
  `itransaction_date` datetime NOT NULL,
  `ipaymentstatus` varchar(60) NOT NULL,
  `ieverything_else` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `qbank`
--

CREATE TABLE `qbank` (
  `qid` int(11) NOT NULL,
  `question` text NOT NULL,
  `description` text NOT NULL,
  `cid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `institute_id` int(11) NOT NULL DEFAULT '1',
  `q_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `qbank`
--

INSERT INTO `qbank` (`qid`, `question`, `description`, `cid`, `did`, `institute_id`, `q_type`) VALUES
(9, '<p>2 + 2 = ?</p>', '', 4, 2, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `question_category`
--

CREATE TABLE `question_category` (
  `cid` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `institute_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `question_category`
--

INSERT INTO `question_category` (`cid`, `category_name`, `institute_id`) VALUES
(4, 'Matematika', 1),
(5, 'Bahasa Inggris', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `quiz`
--

CREATE TABLE `quiz` (
  `quid` int(11) NOT NULL,
  `quiz_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `start_time` int(11) NOT NULL,
  `end_time` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `pass_percentage` varchar(5) NOT NULL,
  `test_type` int(1) NOT NULL,
  `credit` varchar(10) NOT NULL,
  `view_answer` int(1) NOT NULL,
  `max_attempts` int(3) NOT NULL,
  `correct_score` varchar(1000) NOT NULL,
  `incorrect_score` varchar(1000) NOT NULL,
  `institute_id` int(11) NOT NULL DEFAULT '1',
  `qids_static` text,
  `qselect` int(11) NOT NULL DEFAULT '1',
  `ip_address` text NOT NULL,
  `camera_req` int(1) NOT NULL DEFAULT '0',
  `pract_test` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `quiz`
--

INSERT INTO `quiz` (`quid`, `quiz_name`, `description`, `start_time`, `end_time`, `duration`, `pass_percentage`, `test_type`, `credit`, `view_answer`, `max_attempts`, `correct_score`, `incorrect_score`, `institute_id`, `qids_static`, `qselect`, `ip_address`, `camera_req`, `pract_test`) VALUES
(4, 'Aljabar Boolean', '<p>Ini kuis aljabar boolean</p>', 1480530600, 1482431400, 5, '56', 0, '0', 0, 5, '1', '0', 1, '9', 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `quiz_group`
--

CREATE TABLE `quiz_group` (
  `qgid` int(11) NOT NULL,
  `quid` int(11) NOT NULL,
  `gid` int(11) NOT NULL,
  `institute_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `quiz_group`
--

INSERT INTO `quiz_group` (`qgid`, `quid`, `gid`, `institute_id`) VALUES
(21, 4, 11, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `quiz_qids`
--

CREATE TABLE `quiz_qids` (
  `qquid` int(11) NOT NULL,
  `quid` text NOT NULL,
  `cid` text NOT NULL,
  `did` text NOT NULL,
  `no_of_questions` int(11) NOT NULL,
  `institute_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `quiz_result`
--

CREATE TABLE `quiz_result` (
  `rid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `quid` int(11) NOT NULL,
  `qids` text NOT NULL,
  `category_name` varchar(1000) DEFAULT NULL,
  `qids_range` varchar(1000) DEFAULT NULL,
  `oids` text NOT NULL,
  `start_time` int(11) NOT NULL,
  `end_time` int(11) NOT NULL,
  `last_response` int(11) NOT NULL,
  `time_spent` int(11) NOT NULL,
  `time_spent_ind` text NOT NULL,
  `score` float NOT NULL,
  `percentage` varchar(10) NOT NULL DEFAULT '0',
  `q_result` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `institute_id` int(11) NOT NULL DEFAULT '1',
  `photo` text NOT NULL,
  `essay_ques` int(11) NOT NULL DEFAULT '0',
  `score_ind` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `quiz_result`
--

INSERT INTO `quiz_result` (`rid`, `uid`, `quid`, `qids`, `category_name`, `qids_range`, `oids`, `start_time`, `end_time`, `last_response`, `time_spent`, `time_spent_ind`, `score`, `percentage`, `q_result`, `status`, `institute_id`, `photo`, `essay_ques`, `score_ind`) VALUES
(1, 12, 2, '1,2,6,4,3', 'General', '0-4', '2,6-7,11-12-13-14,10,9', 1425394097, 1425394280, 1425394097, 183, '6,5,14,9,31', 4, '80', 1, 1, 1, '', 0, '1,1,1,0,1'),
(2, 12, 1, '1,2,6,4,3,5', 'General', '0-5', '1,6-7,11-12-13-14,10,9,0', 1425394372, 1425394442, 1425394372, 70, '25,3,10,5,5,0', 2, '33.3333333', 1, 1, 1, '1425394372.jpg', 1, '0,1,0,0,0,1'),
(9, 1, 2, '1,2,6,4,3', 'General', '0-4', '0,0,0,10,9', 1480746884, 1480746900, 1480746884, 16, '4,3,1,1,1', 0, '0', 0, 1, 1, '', 0, '0,0,0,0,0'),
(10, 14, 2, '1,2,6,4,3', 'General', '0-4', '1,7,11-13-14-12,10,9', 1480747644, 1480747688, 1480747644, 44, '12,4,8,11,0', 0.5, '10', 0, 1, 1, '', 0, '0,0.5,0,0,0'),
(11, 14, 2, '1,2,6,4,3,8', 'General', '0-5', '0,0,0,10,9,0', 1480747817, 1480747823, 1480747817, 6, '1,0,0,0,0,0', 0, '0', 0, 1, 1, '', 0, '0,0,0,0,0,0'),
(12, 14, 2, '1,2,6,4,3,8', 'General', '0-5', '3,7,13-12-11-14,10,9,0', 1480747865, 1480747915, 1480747865, 50, '11,3,15,5,5,0', 2.5, '41.6666666', 2, 1, 1, '', 1, '0,0.5,1,1,0,0'),
(13, 14, 2, '1,2,6,4,3,8', 'General', '0-5', '4,0,0,10,9,0', 1480750357, 1480750363, 1480750357, 6, '0,0,0,0,0,0', 0, '0', 0, 1, 1, '', 0, '0,0,0,0,0,0'),
(14, 14, 1, '1,2,6,4,3,5', 'General', '0-5', '2,6,11-12-14-13,10,9,0', 1480767042, 1480767106, 1480767042, 64, '7,4,20,11,10,0', 103.5, '1725', 1, 1, 1, '', 1, '1,0.5,1,1,0,100'),
(15, 19, 4, '9', 'Matematika', '0-0', '23', 1480767818, 1480767825, 1480767818, 7, '0', 1, '100', 1, 1, 1, '', 0, '1'),
(16, 19, 4, '9', 'Matematika', '0-0', '23', 1480769303, 1480769314, 1480769303, 11, '3', 1, '100', 1, 1, 1, '', 0, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `q_options`
--

CREATE TABLE `q_options` (
  `oid` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `option_value` text NOT NULL,
  `score` varchar(10) NOT NULL,
  `institute_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `q_options`
--

INSERT INTO `q_options` (`oid`, `qid`, `option_value`, `score`, `institute_id`) VALUES
(21, 9, '<p>2</p>', '0', 1),
(22, 9, '<p>3</p>', '0', 1),
(23, 9, '<p>4</p>', '1', 1),
(24, 9, '<p>5</p>', '0', 1),
(25, 9, '<p>8</p>', '0', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `super_admin`
--

CREATE TABLE `super_admin` (
  `super_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `veri` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `super_admin`
--

INSERT INTO `super_admin` (`super_id`, `email`, `username`, `password`, `veri`) VALUES
(1, 'superadmin@example.com', 'superadmin', '21232f297a57a5a743894a0e4a801fc3', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` tinyint(4) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `credit` varchar(100) NOT NULL DEFAULT '0',
  `gid` int(10) NOT NULL,
  `su` int(1) DEFAULT '1',
  `main_su_admin` int(11) NOT NULL DEFAULT '0',
  `institute_id` int(11) NOT NULL,
  `veri_code` int(11) NOT NULL,
  `noti` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `first_name`, `last_name`, `credit`, `gid`, `su`, `main_su_admin`, `institute_id`, `veri_code`, `noti`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@example.com', 'admin', 'admin', '1000', 11, 1, 1, 1, 0, ''),
(19, 'rendy', '5f4dcc3b5aa765d61d8327deb882cf99', 'gravitycrusher7@yahoo.com.sg', 'Rendy', 'Kurniawan', '0', 11, 0, 0, 1, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_group`
--

CREATE TABLE `user_group` (
  `gid` int(11) NOT NULL,
  `group_name` varchar(200) NOT NULL,
  `institute_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_group`
--

INSERT INTO `user_group` (`gid`, `group_name`, `institute_id`) VALUES
(9, 'DKV 2013', 1),
(10, 'Sistem Informasi 2013', 1),
(11, 'Informatika 2013', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `class_coment`
--
ALTER TABLE `class_coment`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `class_gid`
--
ALTER TABLE `class_gid`
  ADD PRIMARY KEY (`clgid`);

--
-- Indexes for table `difficult_level`
--
ALTER TABLE `difficult_level`
  ADD PRIMARY KEY (`did`);

--
-- Indexes for table `essay_qsn`
--
ALTER TABLE `essay_qsn`
  ADD PRIMARY KEY (`essay_id`);

--
-- Indexes for table `gcm_ids`
--
ALTER TABLE `gcm_ids`
  ADD PRIMARY KEY (`gcm_id`);

--
-- Indexes for table `institute_data`
--
ALTER TABLE `institute_data`
  ADD PRIMARY KEY (`su_institute_id`);

--
-- Indexes for table `live_class`
--
ALTER TABLE `live_class`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`nid`);

--
-- Indexes for table `paypal_ipn`
--
ALTER TABLE `paypal_ipn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qbank`
--
ALTER TABLE `qbank`
  ADD PRIMARY KEY (`qid`);

--
-- Indexes for table `question_category`
--
ALTER TABLE `question_category`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`quid`);

--
-- Indexes for table `quiz_group`
--
ALTER TABLE `quiz_group`
  ADD PRIMARY KEY (`qgid`);

--
-- Indexes for table `quiz_qids`
--
ALTER TABLE `quiz_qids`
  ADD PRIMARY KEY (`qquid`);

--
-- Indexes for table `quiz_result`
--
ALTER TABLE `quiz_result`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `q_options`
--
ALTER TABLE `q_options`
  ADD PRIMARY KEY (`oid`);

--
-- Indexes for table `super_admin`
--
ALTER TABLE `super_admin`
  ADD PRIMARY KEY (`super_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`gid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `class_coment`
--
ALTER TABLE `class_coment`
  MODIFY `content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `class_gid`
--
ALTER TABLE `class_gid`
  MODIFY `clgid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `difficult_level`
--
ALTER TABLE `difficult_level`
  MODIFY `did` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `essay_qsn`
--
ALTER TABLE `essay_qsn`
  MODIFY `essay_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `gcm_ids`
--
ALTER TABLE `gcm_ids`
  MODIFY `gcm_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `institute_data`
--
ALTER TABLE `institute_data`
  MODIFY `su_institute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `live_class`
--
ALTER TABLE `live_class`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `nid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `paypal_ipn`
--
ALTER TABLE `paypal_ipn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `qbank`
--
ALTER TABLE `qbank`
  MODIFY `qid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `question_category`
--
ALTER TABLE `question_category`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `quid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `quiz_group`
--
ALTER TABLE `quiz_group`
  MODIFY `qgid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `quiz_qids`
--
ALTER TABLE `quiz_qids`
  MODIFY `qquid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quiz_result`
--
ALTER TABLE `quiz_result`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `q_options`
--
ALTER TABLE `q_options`
  MODIFY `oid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `super_admin`
--
ALTER TABLE `super_admin`
  MODIFY `super_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `gid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
